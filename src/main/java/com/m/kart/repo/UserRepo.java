package com.m.kart.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.m.kart.entity.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

}
