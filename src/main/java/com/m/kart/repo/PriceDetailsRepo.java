package com.m.kart.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.m.kart.entity.PriceDetails;

@Repository
public interface PriceDetailsRepo extends JpaRepository<PriceDetails ,Long>{

}
