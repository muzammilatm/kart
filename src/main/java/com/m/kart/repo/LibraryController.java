package com.m.kart.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
	public class LibraryController {
	    
	    @Autowired
	    private 	LibraryRepo repository;

	    @GetMapping("/count")
	    public Long findCount() {
	        System.out.println(repository);
	        return repository.count();
	    }
	}


