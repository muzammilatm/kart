package com.m.kart.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.m.kart.entity.Payment;

@Repository
public interface PaymentRepo extends JpaRepository<Payment, Long> {

}
