package com.m.kart.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.m.kart.entity.Orders;

@Repository
public interface OrdersRepo  extends JpaRepository<Orders, Long>{

}



