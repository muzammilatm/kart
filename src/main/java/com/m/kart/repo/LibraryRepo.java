package com.m.kart.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.m.kart.entity.Library;

@Repository
public interface LibraryRepo extends JpaRepository<Library, Long> {

}
