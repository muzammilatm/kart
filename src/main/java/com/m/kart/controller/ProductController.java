package com.m.kart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.m.kart.entity.Product;
import com.m.kart.repo.ProductRepo;

@RestController
public class ProductController { 
	
	@Autowired
	 private ProductRepo productRepo;
	
	@GetMapping(value = "/product")  
	public List<Product> getProduct()   {
		return productRepo.findAll();
	
		
	}
	 
	@PostMapping(value = "/product")  
	public @ResponseBody Product saveProduct(@RequestBody Product product) {
	  	return  productRepo.save(product);
		
	}

}
