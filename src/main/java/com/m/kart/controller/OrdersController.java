package com.m.kart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.m.kart.entity.Orders;
import com.m.kart.repo.OrdersRepo;

@RestController
public class OrdersController {

	@Autowired
	private OrdersRepo ordersRepo;
	
	@GetMapping( "/orders")
	 public List<Orders> getOrders(){
		return  ordersRepo.findAll();
	}
	
	 @PostMapping("/orders")
	  Orders newOrders(@RequestBody Orders Orders) {
	    return ordersRepo.save(Orders);
	 }
	    
	    @DeleteMapping("/orders/{id}")
	    void deleteOrder(@PathVariable Long id) {
	      ordersRepo.deleteById(id);
	  }
	  
	    
	    @PutMapping("/orders/{id}")
	    Orders replaceOrders(@RequestBody Orders Orders, @PathVariable Long id) {
	    	return ordersRepo.findById(id)
	    		      .map(orders -> {
	    		          orders.setName(Orders.getName());
	    		          orders.setId(Orders.getId());
	    		          return ordersRepo.save(orders);
	    		        })
	    		        .orElseGet(() -> {
	    		          Orders.setId(id);
	    		          return ordersRepo.save(Orders);
	    		        });
	    		    }
	    }
	      
	
	    	

;