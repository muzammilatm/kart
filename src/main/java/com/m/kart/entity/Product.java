package com.m.kart.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Product  {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name="product_id")
	private List<PriceDetails> priceDetails;
	

	@OneToMany(fetch =FetchType.LAZY)
	@JoinColumn(name ="product_id")
	private List<User>user;

	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<User> getUser() {
		return user;
	}

	public void setUser(List<User> user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Product(int i, String string) {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<PriceDetails> getPriceDetails() {
		return priceDetails;
	}

	public void setPriceDetails(List<PriceDetails> priceDetails) {
		this.priceDetails = priceDetails;
	}

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

}
