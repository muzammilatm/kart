package com.m.kart;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import services.Search;

@Profile("test")
@Configuration
public class MockConfig {
   @Bean
   @Primary
   public Search search() {
      return Mockito.mock(Search.class);
   }
}