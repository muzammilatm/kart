package com.m.kart;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.m.kart.entity.Product;
import com.m.kart.repo.ProductRepo;

import services.Search;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class SearchProductTest {
	
   @Autowired
   private Search search;
	
  @Autowired
   private ProductRepo productRepo;
 
    @Test
	public void getProductFromSearchTest() {
	 Mockito.when(productRepo.findAll()).thenReturn(Stream.of(new Product(1,"Levis"),
				new Product(2,"Puma")).collect(Collectors.toList()));
	 List<Product> result = search.getProduct();
	 Assert.assertEquals(result.get(0).getName(),"Levis");
	}
	}